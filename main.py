
# -*- coding: utf-8 -*-
import os
import re
import traceback
import time
import sys
from datetime import datetime
from appdata.measurmentStruct import *
from appdata.sensorDefs import *
from utility import *
#os.umask(0)
INTERVAL = 5
DATABASE = 'LocalofficeDB'
SENSOR_ID_LIST = [BH1750_ID, BMP180_ID, AM2320_ID]#, 'serialUSB','Custom']
SENSOR_OBJ = []
#Collect sensors to list of objects
ms = measurmentStruct(DATABASE)
ms.Name = 'Enviro_station'

for sensor_id in SENSOR_ID_LIST:
    if sensor_id == BH1750_ID:
        obj = BH1750sensor()
        SENSOR_OBJ.append(obj)
    elif sensor_id == BMP180_ID:
        SENSOR_OBJ.append(BMP180sensor())
    elif sensor_id == AM2320_ID:
        SENSOR_OBJ.append(AM2320sensor())
    #elif sensor_id == CUSTOM_ID:
        #SENSOR_OBJ.append(BMP180sensor())
    



#Start Endless measuring 
while True:
    for sensor in SENSOR_OBJ:
        res = sensor.get_measure()
        ms.codeList.extend(res.keys())
        ms.measurments.extend(res.values())
    ms.saveMeasurment('localhost')
    time.sleep(INTERVAL)

    


#Collector = modbusCollector(deviceName = 'USB2.0-Serial')
#Collector.ms.Name = "Office climat v2"
#Collector.servIp = 'localhost'
#Collector.execRead()
        
    
      
