
import time
import sys
from influxdb import InfluxDBClient
from datetime import datetime
import platform
import subprocess #для перезагрузки Linux
import serial
import serial.tools.list_ports #Для чтения описания устройства
from appdata.ModbusClientMod import *
from appdata.measurmentStruct import *
import utility

class modbusSensor(ModbusClient):
    def __init__(self, comport = "", deviceName = ""):
        super(modbusCollector, self).__init__(comport)
        self.comportDefined = False
        self._deviceName = deviceName
        if comport != "":
            self.comportDefined = True
        self._comport = comport
        self._baudrate = 9600
        self._parity = 1
        self._stopbits = 1
        self.ms = measurmentStruct('Test_DataBase')
        self.servIp = 'localhost'
        self.servPort = 8086
        self.UnitIdentifier = 1
        self.portOpened = False
    
    def Connect(self):
        err = False
        
        #Проверяем есть ли явное определение ком порта
        if not self.comportDefined:
            #Определяем порт по имени устройства
            self._comport = ""
            if self._deviceName == "":
                raise Exception('Com port device not defined')
            ComDeviceFound = False
            comlist = serial.tools.list_ports.comports()
            
            for n in comlist:
                if self._deviceName in str(n.description):
                    self._comport = str(n.device)
                    ComDeviceFound = True
                    #print(COMportStr)
                    break
            if not ComDeviceFound:
                LogString("Could't find device")
                if os.path.exists('DeviceErr.txt'):
                    LogString("Start Waiting for device...")
                    #self.waitForDevice()
                else:
                    print('Reboot')
                    errF = open('DeviceErr.txt', 'w')
                    errF.write('error')
                    errF.close()
                    if 'Linux' in platform.system():
                        subprocess.check_call(['sudo', 'reboot'])
                        #softreset = subprocess.Popen(['systemctl', 'reboot']) #Перезагрузка
                        time.sleep(10)
            
        #Проверка определения порта
        if self._comport != "":
            for i in range(3):
                try:
                    self.ser = serial.Serial(self._comport, self._baudrate, timeout=1, parity=serial.PARITY_NONE, stopbits=self._stopbits, xonxoff=0, rtscts=0) #Нужна доработка с тестированием!!!!!!!!!!!!!
                    err = False
                    self.portOpened = True
                    break
                except Exception as e:
                    print('Ошибка:\n', traceback.format_exc())
                    err = True
                    LogString('Com port opening error')
                    time.sleep(5)#Ожидание 5 секунды перед повторным соединением
            if err:
                print('Reboot')
                if 'Linux' in platform.system():
                    subprocess.check_call(['sudo', 'reboot'])
                    time.sleep(10)
        
            
    def execRead(self):
        msg = ""
        time.sleep(5)
        self.Connect()
        if self.portOpened == True:
            #Запуск чтения по Modbus Доработать добавление тегов главной программе а не хардкодом
            try:
                req = self.ReadHoldingRegisters(8, 2)
                CO2 = ConvertRegistersToFloat(req)
                self.ms.codeList.append('CO2')
                self.ms.measurments.append(round(CO2[0], 0))
            except Exception as e:
                LogString(str(e))
            try:
                req = self.ReadHoldingRegisters(4, 2)
                Hum = ConvertRegistersToFloat(req)
                self.ms.codeList.append('Humidity')
                self.ms.measurments.append(round(Hum[0], 2))
            except Exception as e:
                LogString(str(e))
            try:
                req = self.ReadHoldingRegisters(0, 2)
                Temp = ConvertRegistersToFloat(req)
                self.ms.codeList.append('Temp')
                self.ms.measurments.append(round(Temp[0], 1))
            except Exception as e:
                LogString(str(e))
            self.ms.saveMeasurment(self.servIp, self.servPort)
            self.close()
            self.portOpened = False
        else:
            LogString("Port isn't opened stop requests")
            
#    def waitForDevice(self):
#        deviceFound = False
#        while not deviceFound:
#            time.sleep(10) #Ждать 5 секунд
#            if self._deviceName == "":
#                raise Exception('Com port device not defined')
#            COMportStr = ""
#            comlist = serial.tools.list_ports.comports()
#            for n in comlist:
#                if self._deviceName in str(n.description):
#                    self._comport = str(n.device)
#                    deviceFound = True
#                    os.remove('DeviceErr.txt')
#                    LogString('Device founded')
                    #print(COMportStr)
#                    break
                
            
        
    def lineProc(self, line):
        res = re.search('(?<=abc)def', line)
        timeres = re.search('32432', line)
        reslist = res.groups()
        self.ms.TimeStamp = timeres.group(0)    #Доделать форматирование времени
        self.TimeCorrection(self.ms.TimeStamp)  #Отсылка сообщения для коррекции
        #Заполнение полученных значений
        for i in range(len(reslist)):
            self.ms.codeList.append('Arg'+str(i))
            self.ms.measurments.append(reslist[i])
        self.TimeCorrection(self.ms.TimeStamp) #Коррекция времени
        ms.saveMeasurment(self.servIp, self.servPort)
        return 0
    def TimeCorrection(self, Time): #Функция корректировки времени
        pass