#Sensors imports
from appdata.BH1750 import *
from appdata.BMP180 import *
from appdata.AM2320 import *

#from appdata.serialUSBsensor import *
#from appdata.customRadio import *

#Sensor ID's
BH1750_ID = 1
BMP180_ID = 2
AM2320_ID = 3
CUSTOM_ID = 55

#Re identification for sensor commands    
      
class BH1750sensor(BH1750_sens):
    def __init__(self):
        super().__init__()

    def get_measure(self):
        meas = {'lum': self.measure_high_res2()}
        return meas

class BMP180sensor(BMP180):
    def __init__(self):
        super().__init__()

    def get_measure(self):
        meas = {'barP': (self.read_pressure()/100), 'tempPsens': self.read_temperature()}
        return meas

class AM2320sensor(AM2320):
    def __init__(self):
        super().__init__(1)

    def get_measure(self):
        measList = self.readSensor()
        meas = {'Temp': measList[0], 'Hum': measList[1]}
        return meas


        
"""
class serialUSBsensor(modbusSensor):
    def __init__(self):
        super(modbusSensor, self).__init__()
    def get_measure():
        pass
    

class RadioSensor(customRadio)
    def __init__(self):
        super(customRadio, self).__init__()
    def get_measure():
        pass
"""