# Distributed with a free-will license.
# Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
# BMP180

import smbus
import time
# BMP180 default address.
BMP180_I2CADDR           = 0x77

# Operating Modes
BMP180_ULTRALOWPOWER     = 0
BMP180_STANDARD          = 1
BMP180_HIGHRES           = 2
BMP180_ULTRAHIGHRES      = 3

# BMP180 Registers
BMP180_CAL_REG_POINTER           = 0xAA  # R   Calibration data pointer

BMP180_CONTROL           = 0xF4
BMP180_DATA_REG          = 0xF6

# Commands
BMP180_READTEMPCMD       = 0x2E
BMP180_READPRESSURECMD   = 0x34

class BMP180:
    def __init__(self, mode=BMP180_STANDARD, address=BMP180_I2CADDR):
        # Check that mode is valid.
        if mode not in [BMP180_ULTRALOWPOWER, BMP180_STANDARD, BMP180_HIGHRES, BMP180_ULTRAHIGHRES]:
            raise ValueError('Unexpected mode value {0}.  Set mode to one of BMP180_ULTRALOWPOWER, BMP180_STANDARD, BMP180_HIGHRES, or BMP180_ULTRAHIGHRES'.format(mode))
        self._mode = mode
        self._address = address
        # Create I2C device.
        self.bus = smbus.SMBus(1)
        # Load calibration values.
        self._load_calibration()

    def _load_calibration(self):
        data = self.bus.read_i2c_block_data(self._address, BMP180_CAL_REG_POINTER, 22) #22 - Number of data in calibration register

        # Convert the data
        self.cal_AC1 = data[0] * 256 + data[1]
        if self.cal_AC1 > 32767 :
            self.cal_AC1 -= 65535
        self.cal_AC2 = data[2] * 256 + data[3]
        if self.cal_AC2 > 32767 :
            self.cal_AC2 -= 65535
        self.cal_AC3 = data[4] * 256 + data[5]
        if self.cal_AC3 > 32767 :
            self.cal_AC3 -= 65535
        self.cal_AC4 = data[6] * 256 + data[7]
        self.cal_AC5 = data[8] * 256 + data[9]
        self.cal_AC6 = data[10] * 256 + data[11]
        self.cal_B1 = data[12] * 256 + data[13]
        if self.cal_B1 > 32767 :
            self.cal_B1 -= 65535
        self.cal_B2 = data[14] * 256 + data[15]
        if self.cal_B2 > 32767 :
            self.cal_B2 -= 65535
        self.cal_MB = data[16] * 256 + data[17]
        if self.cal_MB > 32767 :
            self.cal_MB -= 65535
        self.cal_MC = data[18] * 256 + data[19]
        if self.cal_MC > 32767 :
            self.cal_MC -= 65535
        self.cal_MD = data[20] * 256 + data[21]
        if self.cal_MD > 32767 :
            self.cal_MD -= 65535

    def _load_datasheet_calibration(self):
        # Set calibration from values in the datasheet example.  Useful for debugging the
        # temp and pressure calculation accuracy.
        self.cal_AC1 = 408
        self.cal_AC2 = -72
        self.cal_AC3 = -14383
        self.cal_AC4 = 32741
        self.cal_AC5 = 32757
        self.cal_AC6 = 23153
        self.cal_B1 = 6190
        self.cal_B2 = 4
        self.cal_MB = -32767
        self.cal_MC = -8711
        self.cal_MD = 2868

    def read_raw_temp(self):
        """Reads the raw (uncompensated) temperature from the sensor."""
        self.bus.write_byte_data(self._address, BMP180_CONTROL, BMP180_READTEMPCMD)
        time.sleep(0.005)  # Wait 5ms
        data = self.bus.read_i2c_block_data(self._address, BMP180_DATA_REG, 2)
        raw = (data[0]<<8) + data[1]
        return raw

    def read_raw_pressure(self):
        """Reads the raw (uncompensated) pressure level from the sensor."""
        self.bus.write_byte_data(self._address, BMP180_CONTROL, BMP180_READPRESSURECMD + (self._mode << 6))
        if self._mode == BMP180_ULTRALOWPOWER:
            time.sleep(0.005)
        elif self._mode == BMP180_HIGHRES:
            time.sleep(0.014)
        elif self._mode == BMP180_ULTRAHIGHRES:
            time.sleep(0.026)
        else:
            time.sleep(0.008)
	
        msb = self.bus.read_byte_data(self._address, BMP180_DATA_REG)
        lsb = self.bus.read_byte_data(self._address, BMP180_DATA_REG+1)
        xlsb = self.bus.read_byte_data(self._address, BMP180_DATA_REG+2)
        raw = ((msb << 16) + (lsb << 8) + xlsb) >> (8 - self._mode)
        #self._logger.debug('Raw pressure 0x{0:04X} ({1})'.format(raw & 0xFFFF, raw))
        return raw

    def read_temperature(self):
        """Gets the compensated temperature in degrees celsius."""
        UT = self.read_raw_temp()
        # Datasheet value for debugging:
        #UT = 27898
        # Calculations below are taken straight from section 3.5 of the datasheet.
        X1 = ((UT - self.cal_AC6) * self.cal_AC5) >> 15
        X2 = (self.cal_MC << 11) // (X1 + self.cal_MD)
        B5 = X1 + X2
        temp = ((B5 + 8) >> 4) / 10.0
        #self._logger.debug('Calibrated temperature {0} C'.format(temp))
        return temp

    def read_pressure(self):
        """Gets the compensated pressure in Pascals."""
        UT = self.read_raw_temp()
        UP = self.read_raw_pressure()
        # Datasheet values for debugging:
        #UT = 27898
        #UP = 23843
        # Calculations below are taken straight from section 3.5 of the datasheet.
        # Calculate true temperature coefficient B5.
        X1 = ((UT - self.cal_AC6) * self.cal_AC5) >> 15
        X2 = (self.cal_MC << 11) // (X1 + self.cal_MD)
        B5 = X1 + X2
        #self._logger.debug('B5 = {0}'.format(B5))
        # Pressure Calculations
        B6 = B5 - 4000
        #self._logger.debug('B6 = {0}'.format(B6))
        X1 = (self.cal_B2 * (B6 * B6) >> 12) >> 11
        X2 = (self.cal_AC2 * B6) >> 11
        X3 = X1 + X2
        B3 = (((self.cal_AC1 * 4 + X3) << self._mode) + 2) // 4
        #self._logger.debug('B3 = {0}'.format(B3))
        X1 = (self.cal_AC3 * B6) >> 13
        X2 = (self.cal_B1 * ((B6 * B6) >> 12)) >> 16
        X3 = ((X1 + X2) + 2) >> 2
        B4 = (self.cal_AC4 * (X3 + 32768)) >> 15
        #self._logger.debug('B4 = {0}'.format(B4))
        B7 = (UP - B3) * (50000 >> self._mode)
        #self._logger.debug('B7 = {0}'.format(B7))
        if B7 < 0x80000000:
            p = (B7 * 2) // B4
        else:
            p = (B7 // B4) * 2
        X1 = (p >> 8) * (p >> 8)
        X1 = (X1 * 3038) >> 16
        X2 = (-7357 * p) >> 16
        p = p + ((X1 + X2 + 3791) >> 4)
        #self._logger.debug('Pressure {0} Pa'.format(p))
        return p

    def read_altitude(self, sealevel_pa=101325.0):
        """Calculates the altitude in meters."""
        # Calculation taken straight from section 3.6 of the datasheet.
        pressure = float(self.read_pressure())
        altitude = 44330.0 * (1.0 - pow(pressure / sealevel_pa, (1.0/5.255)))
        return altitude

    def read_sealevel_pressure(self, altitude_m=0.0):
        """Calculates the pressure at sealevel when given a known altitude in
        meters. Returns a value in Pascals."""
        pressure = float(self.read_pressure())
        p0 = pressure / pow(1.0 - altitude_m/44330.0, 5.255)
        return p0



#sensor = BMP180(mode=BMP180_ULTRAHIGHRES)
#print('P = ' + str(sensor.read_pressure()))
#print('T = ' + str(sensor.read_temperature()))