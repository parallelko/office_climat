import time
from influxdb import InfluxDBClient
from datetime import datetime
import re
import traceback
from utility import *

class measurmentStruct:
    def __init__(self, database = 'Main'):
        self.dataBase = database 
        self.Name = ""
        self.TimeStamp = ""
        self.codeList = []
        self.measurments = []
        self.error = ''
    def saveMeasurment(self, servIp = 'localhost', servPort = 8086):
        #___________________________________________________debugPrints ____________________________________________________________
        #print(self.TimeStamp)
        #print(self.Name)
        #print(self.FWVersion)
        #print(self.serialNumber)
        #print(self.codeList)
        #print(self.measurments)
        #print("Create database: " + dataBase)
        #self.inflDBclient.drop_database(self.dataBase)
        #print("Write data")
        if self.TimeStamp == "": #If time no setted
            timestr = str(datetime.utcnow())
            #print(timestr)
            i = 0
            timeAct = ""
            for i in range(len(timestr)):
                """if timestr[i] == ".":
                measur.TimeStamp = timeAct + 'Z'
                break"""
                if timestr[i] == ' ':
                    timeAct += 'T'
                else:
                    timeAct += timestr[i]
            self.TimeStamp = timeAct + 'Z'
        #Filling fields
        if len(self.codeList) == len(self.measurments): #Checking for correct filling fields
            #Filling points
            fields = {}
            for n in range(len(self.codeList)):
                try:
                    fValue = float(self.measurments[n])
                    fields.update({self.codeList[n]: fValue})
                except Exception as e:
                    print('Error:\n', traceback.format_exc())
                #fields.update({"Name": meas.Name})
                #fields.update({"FW Version": meas.FWVersion})
            #Writing errors
            if self.error != "":
                fields.update({'sys_error': self.error})
            #print("Write fields: {0}".format(fields))
            #Make json struct for writing
            json_body = [{"measurement": self.Name, "time": self.TimeStamp, "fields": fields}]
            #print("Write points: {0}".format(json_body))
            try:
                inflDBclient = InfluxDBClient(servIp, servPort, 'root', 'root', self.dataBase)
                inflDBclient.create_database(self.dataBase)
                inflDBclient.write_points(json_body)
            except Exception as e:              #Do normal catch exceptions
                #LogString('InfluxDB writing error')
                print('Error:\n', traceback.format_exc())
                
            #print("__________________________________________________________________")
            self.clear()
    def clear(self):
        self.codeList.clear()
        self.measurments.clear()
        self.TimeStamp = ""
        self.error = ''
        #self.Name = ""
